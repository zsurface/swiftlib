import SpriteKit
import Foundation
import Swift

struct Constance{
    static let SUM10 = 10
    static let SUM20 = 20
    static let SUM30 = 30
}


// Affine space definition
// f:(Pt + Vec) -> Pt
class Pt{
    var h:Int
    var w:Int
    init(){
        self.h = 0
        self.w = 0
    }
    init(_ h:Int, _ w:Int){
        self.h = h
        self.w = w
    }
    init(arr:[Int]){
        self.h = arr[0] 
        self.w = arr[1] 
    }
    static func +(left:Pt, right:Pt)->Pt{
        return Pt(left.h + right.h, left.w + right.w)
    }
    
    // (x1, y1) + <x2, y2> = (x1 + x2, y1 + y2)
    // <2, 3> = (x1 + x2, y1 + y2) - (x1, y1)
    static func +(left:Pt, right:Vec)->Pt{
        return Pt(left.h + right.dir.h, left.w + right.dir.w)
    }
    static func -(left:Pt, right:Pt)->Vec{
        return   Vec(right, left)
    }
    static func ==(left:Pt, right:Pt)->Bool{
        return left.h == right.h && left.w == right.w
    }
    static prefix func -(pt:Pt)->Pt{
        return Pt(-pt.h, -pt.w)
    }
}
class Vec{
    var p1:Pt = Pt()
    var p2:Pt = Pt()
    var dir:Pt = Pt()
    init(){
    }
    init(_ p1:Pt, _ p2:Pt){
        self.p1 = p1
        self.p2 = p2
        dir = Pt(p2.h - p1.h, p2.w - p1.w)
    }
    init(_ p:Pt){
        let pp1 = Pt()
        let pp2 = p
        dir = Vec(pp1, pp2).dir
    }
    static prefix func -(_ v:Vec)->Vec{
        return Vec(Pt(), -v.dir)
    }
    static func ==(left:Vec, right:Vec)->Bool{
        return left.dir.h == right.dir.h && left.dir.w == right.dir.w
    }
}


func drop<T>(num:Int, list:[T]) -> [T]{
    var ls : [T] = []
    for inx in 0..<list.count{
        if inx > (num - 1){
            ls.append(list[inx])
        }
    }
    return ls
}

func take<T>(num:Int, list:[T]) -> [T]{
    var ls : [T] = []
    for inx in 0..<list.count{
        if inx < num{
            ls.append(list[inx])
        }
    }
    return ls
}

func splitFirst<T>(list:[T]) -> (T, [T])?{
    if list.isEmpty{
        return nil
    }else{
        return (list[0], drop(num: 1, list: list))
    }
}
func tails<T>(list:[T]) -> [[T]] {
    if list.isEmpty{
        return [[]]
    }else{
        let rest = drop(num:1, list:list)
        var ls :[[T]] = tails(list:rest)
        ls.insert(list, at: 0)
        return ls
    }
}
func combine<T>(num:Int, list:[T]) -> [[T]] {
    if num == 0 {
        return [[]]
    }else{
        var ret : [[T]] = []
        for ls in  tails(list: list){
            if let (x, cx) = splitFirst(list:ls){
                for cs in combine(num: num - 1, list: cx){
                    var ce : [T] = cs
                    ce.insert(x, at: 0)
                    ret.append(ce)
                }
            }
        }
        return ret
    }
}

// create w element 1d array
public func creatArray(w:Int)->[Int]{
    return [Int](repeating:0, count: w)
}

// create [h:height, w:width] 2d array
public func creatArray(h:Int, w:Int)->[[Int]]{
    return [[Int]](repeating:[Int](repeatElement(0, count: w)), count: h)
}


public func random(_ n:Int)->Int{
    return Int(arc4random_uniform(UInt32(n)))
}

// gene pair random num: (3, 4)
public func randomPair(h:Int, w:Int)->(h:Int, w:Int){
    let rh = arc4random_uniform(UInt32(h))
    let rw = arc4random_uniform(UInt32(w))
    return (Int(rh), Int(rw))
}

/*
  let pt = randomPt(x : -10...10, y:-10...10)
  print("\(pt)")

 */
func randomPt(x: ClosedRange<Int>, y:ClosedRange<Int>) -> CGPoint{
    return CGPointMake(CGFloat(Int.random(in:x)), CGFloat(Int.random(in:y)))
}

func randomPtRect(topLeft:CGPoint, width:CGFloat, height:CGFloat) -> CGPoint{
    let rx = CGFloat.random(in: 0...1.0)
    let ry = CGFloat.random(in: 0...1.0)
    return CGPoint(x : topLeft.x + width * rx, y : topLeft.y - height * ry)
}
/*
 KEY: random point
 */
func randomPairPt(e0:CGPoint, e1:CGPoint, width:CGFloat) -> (CGPoint, CGPoint){
    let h = abs(e1.y - e0.y)
    let c0 = randomPtRect(topLeft:CGPoint(x:e0.x - width/2, y:e0.y), width: width, height:h)
    let c1 = randomPtRect(topLeft:CGPoint(x:e0.x - width/2, y:e0.y), width: width, height:h)
    return (c0, c1)
}

/*
 KEY: random points, random list points
 */
func randomPtRectList(topLeft:CGPoint, width:CGFloat, height:CGFloat, count:Int) -> [CGPoint]{
    var ls:[CGPoint] = []
    for _ in 1...count{
        let px = randomPtRect(topLeft:topLeft, width:width, height:height)
        ls.append(px)
    }
    return ls
}


class Grid{
    private var heightNum       = 10
    private var widthNum        = 10
    private var gridArrInt      = [[Int]]()
    public var gridNodeArr2d    = [[NodeMarker?]]()
    private var gridCenterArr2d = [[CGPoint]]()
    private let lock            = DispatchSemaphore(value: 1)
    private let queue           = DispatchQueue(label: "gridNodeArr2d")
    private var unitWidth:CGFloat = 0.0
    private var unitHeight:CGFloat = 0.0

    // let unitWidth:CGFloat = frame.width/CGFloat(numCols)
    // let unitHeight:CGFloat = frame.height/CGFloat(numRows)
    
    init(){
    }
    init(heightNum:Int, widthNum:Int, frame:CGRect) {
        self.heightNum = heightNum
        self.widthNum = widthNum
        self.unitHeight = frame.height/CGFloat(heightNum)
        self.unitWidth = frame.width/CGFloat(widthNum)
        gridCenterArr2d = gridPoint(rowLen:self.widthNum, colLen: self.heightNum, frame:frame)
        //gridNodeArr2d = geneBlockArr(heightNum: self.heightNum, widthNum: self.widthNum)
        gridNodeArr2d = fillGrid(heightNum: self.heightNum, widthNum: self.widthNum)
    }

    // KEY: add rectangle
    private func fillGrid(heightNum:Int, widthNum:Int)->[[NodeMarker?]]{
        var arr2d = [[NodeMarker?]]()
        for h in 0..<heightNum{
            var tmpArr = [NodeMarker?]()
            for w in 0..<widthNum{
                let pos = gridCenterArr2d[h][w]
                
                // XXX2:
                // tmpArr.append(NodeMarker(isNew:true, isGray:false, block: BlockNode(position:pos, num:0, size:CGSize(width: widthNum, height: heightNum))))
                tmpArr.append(NodeMarker(isNew:true, isGray:false, block: BlockNode(position:pos, num:0, size:CGSize(width:unitWidth, height:unitHeight))))
            }
            arr2d.append(tmpArr)
        }
        return arr2d
    }

    private func geneBlockArr(heightNum:Int, widthNum:Int)->[[NodeMarker?]]{
        var arr2d = [[NodeMarker?]]()
        for _ in 0..<heightNum{
            var tmpArr = [NodeMarker?]()
            for _ in 0..<widthNum{
                tmpArr.append(NodeMarker())
            }
            arr2d.append(tmpArr)
        }
        return arr2d
    }
    
    func fillGrid(){}
    
    func initGridInt(arr2d:[[Int]]){
        self.heightNum = arr2d.count
        self.widthNum = arr2d[0].count
        for h in 0..<heightNum{
            gridArrInt.append(arr2d[h])
        }
        print(gridArrInt)
        for h in 0..<heightNum{
            for w in 0..<widthNum{
                if gridArrInt[h][w] == 1{
                    gridNodeArr2d[h][w]?.block = BlockNode(position:self.gridCenterArr2d[h][w], num: 3, size: CGSize(width:unitWidth, height:unitHeight))
                }
            }
        }
    }

    // Check whether there is any empty spot in column: indexW
    public func isAnyEmptySpot(indexW:Int)->Bool{
        var conBlocksNum:Int = 0
        var notDone:Bool = true
        for h in 0..<heightNum where notDone {
            queue.sync{
                if gridNodeArr2d[h][indexW]?.isHidden() == false{
                    conBlocksNum += 1
                }else{
                    notDone = false
                }
            }
        }
        
        var allBlocksNum:Int = 0
        for h in 0..<heightNum{
            queue.sync{
                if gridNodeArr2d[h][indexW]?.isHidden() == false{
                    allBlocksNum += 1
                }
            }
        }
        return conBlocksNum != allBlocksNum
    }
    
    public func getEmptyMap()->[Int:[Int]]{
        var hiddenMap = [Int:[Int]]()
        for w in 0..<widthNum{
            var list = [Int]()
            var top = -1
            
            for h in (0..<heightNum).reversed() where top == -1 {
                if !(gridNodeArr2d[h][w]?.block?.labelNode.isHidden)!{
                    top = h
                }
            }
            if top != -1{
                for h in 0..<heightNum {
                    if h < top && (gridNodeArr2d[h][w]?.block?.labelNode.isHidden)!{
                        list.append(h)
                    }
                }
                if list.count > 0{
                    hiddenMap[w] = list
                }
            }
        }
        return hiddenMap
    }
    
    // Check whether any empty spot in the whole grid
    public func doesGridContainEmptySpot()->Bool{
        var ret:Bool = false
        let list = self.columnContainsEmptySpot()
        for w in 0..<self.widthNum where !ret {
            if list[w] != 0{
                ret = true
            }
        }
        return ret
    }
    
    // Check whether the grid is filled with blocks fully
    public func isGridFull()->Bool{
        var ret:Bool = true
        for h in 0..<heightNum where ret{
            for w in 0..<widthNum where ret{
                queue.sync{
                    if gridNodeArr2d[h][w]?.isHidden() == true{
                        ret = false
                    }
                }
            }
        }
        return ret
    }
    
    // Count the number of node
    public func countBlock()->Int{
        var count = 0
        for h in 0..<heightNum{
            for w in 0..<widthNum{
                queue.sync{
                    if gridNodeArr2d[h][w]?.isHidden() == false{
                        count += 1
                    }
                }
            }
        }
        return count
    }
    
    // Return [columnIndex : num of empty spot]
    public func columnContainsEmptySpot()->[Int]{
        var arr = [Int]()
        for w in 0..<widthNum {
            // count the empty block from up [heightNum-1...]
            var upNum = 0
            var upDone:Bool = true
            for h in 0..<heightNum where upDone{
                let rh = (heightNum - 1) - h
                queue.sync {
                    if gridNodeArr2d[rh][w]?.isHidden() == true{
                        upNum += 1
                    }else{
                        upDone = false
                    }
                }
            }
        
            // count all the non-empty block
            var boxNum = 0
            for h in 0..<heightNum {
                queue.sync{
                    if gridNodeArr2d[h][w]?.isHidden() == false{
                        boxNum += 1
                    }
                }
            }
            arr.append(heightNum - (upNum + boxNum))
        }
        return arr
    }
    
    func printGrid(){
        var arr2d = creatArray(h: heightNum, w: widthNum)
        print("---------------------------------------")
        for h in 0..<heightNum {
            for w in 0..<widthNum {
                // arr2d[h][w] = gridNodeArr2d[h][w]?.isHidden() == true ? 0 : 1
                arr2d[h][w] = Int((gridNodeArr2d[h][w]?.block?.labelNode.text)!)!
            }
        }
        let flipArr = flipColumn(arr: arr2d)
        for h in 0..<heightNum {
            for w in 0..<widthNum {
                print(String(format: "[%d]", flipArr[h][w]), terminator:"")
            }
            print("")
        }
        print("---------------------------------------")
    }
    
    func printGridHidden(){
        for h in (0..<heightNum).reversed() {
            for w in 0..<widthNum {
                print(String(format: "[%d,%d]", h, w), terminator:"")
            }
            print("")
        }
        print("---------------------------------------")
        
        for h in (0..<heightNum).reversed() {
            for w in 0..<widthNum {
                print(String(format: "[%@]", String((gridNodeArr2d[h][w]?.block?.labelNode.isHidden)!)), terminator:"")
                // print(String((gridNodeArr2d[h][w]?.block?.labelNode.isHidden)!))
            }
            print("")
        }
        print("---------------------------------------")
    }
    
    
    // Fill all the grids with nodes
    func testGeneFullGrid(){
        for h in 0..<self.heightNum{
            for w in 0..<self.widthNum{
                self.gridNodeArr2d[h][w]?.block = BlockNode(position:self.gridCenterArr2d[h][w], num: 3, size: CGSize(width: 40, height: 40))
            }
        }
    }
    
    func testFullBottomRow(){
        for w in 0..<self.widthNum{
            self.gridNodeArr2d[0][w]?.block = BlockNode(position:self.gridCenterArr2d[0][w], num: 3, size: CGSize(width: 40, height: 40))
            
        }
    }
    
    func testNotSum10Twoblocks(){
        for w in 0..<self.widthNum{
            self.gridNodeArr2d[0][w]?.block = BlockNode(position:self.gridCenterArr2d[0][w], num: 3, size: CGSize(width: 40, height: 40))
            self.gridNodeArr2d[2][w]?.block = BlockNode(position:self.gridCenterArr2d[2][w], num: 3, size: CGSize(width: 40, height: 40))
        }
    }

    func testSum10TwoBlocks(){
        self.gridNodeArr2d[0][0]?.block = BlockNode(position:self.gridCenterArr2d[0][0], num: 3, size: CGSize(width: 40, height: 40))
        self.gridNodeArr2d[0][1]?.block = BlockNode(position:self.gridCenterArr2d[0][1], num: 7, size: CGSize(width: 40, height: 40))
        
    }
    
    func testSum10ThreeBlocks(){
        self.gridNodeArr2d[0][0]?.block = BlockNode(position:self.gridCenterArr2d[0][0], num: 1, size: CGSize(width: 40, height: 40))
        self.gridNodeArr2d[0][1]?.block = BlockNode(position:self.gridCenterArr2d[0][1], num: 4, size: CGSize(width: 40, height: 40))
        self.gridNodeArr2d[0][2]?.block = BlockNode(position:self.gridCenterArr2d[0][2], num: 5, size: CGSize(width: 40, height: 40))
    }
    
    func testSum10ShareBlocks1(){
        self.gridNodeArr2d[0][0]?.block = BlockNode(position:self.gridCenterArr2d[0][0], num: 1, size: CGSize(width: 40, height: 40))
        self.gridNodeArr2d[0][1]?.block = BlockNode(position:self.gridCenterArr2d[0][1], num: 4, size: CGSize(width: 40, height: 40))
        self.gridNodeArr2d[0][2]?.block = BlockNode(position:self.gridCenterArr2d[0][2], num: 5, size: CGSize(width: 40, height: 40))
        self.gridNodeArr2d[0][3]?.block = BlockNode(position:self.gridCenterArr2d[0][2], num: 5, size: CGSize(width: 40, height: 40))
    }
    
    func testSum10ShareBlocks2(){
        self.gridNodeArr2d[0][0]?.block = BlockNode(position:self.gridCenterArr2d[0][0], num: 1, size: CGSize(width: 40, height: 40))
        self.gridNodeArr2d[0][1]?.block = BlockNode(position:self.gridCenterArr2d[0][1], num: 8, size: CGSize(width: 40, height: 40))
        self.gridNodeArr2d[0][2]?.block = BlockNode(position:self.gridCenterArr2d[0][2], num: 1, size: CGSize(width: 40, height: 40))
        self.gridNodeArr2d[0][3]?.block = BlockNode(position:self.gridCenterArr2d[0][3], num: 9, size: CGSize(width: 40, height: 40))
    }
} // End class Grid

public class Log{
    class func e(_ object:Any, // 1
        fileName:String=#file, // 2
        line:Int = #line, // 3
        column:Int=#column, // 4
        funcName:String = #function){
        print("\(line):\(funcName)->\(object)")
    }
    
}

// flip the column of [[Int]]
//
// [3] => [1]
// [1] => [3]
public func flipColumn(arr:[[Int]])->[[Int]]{
    let heightNum = arr.count
    let widthNum = arr[0].count
    var tmpArr = [[Int]](repeating:[Int](repeatElement(0, count: widthNum)), count: heightNum)
    
    for w in 0..<widthNum {
        for h in  0..<heightNum {
            tmpArr[h][w] = arr[h][w]
        }
    }
    for w in 0..<widthNum {
        for h in 0..<heightNum/2 {
            let rh = heightNum - 1 - h
            let tmp = tmpArr[h][w]
            tmpArr[h][w] = tmpArr[rh][w]
            tmpArr[rh][w] = tmp
        }
    }
    return tmpArr
}

func geneBlockArr(heightNum:Int, widthNum:Int)->[[NodeMarker?]]{
    var arr2d = [[NodeMarker?]]()
    for _ in 0..<heightNum{
        var tmpArr = [NodeMarker?]()
        for _ in 0..<widthNum{
            tmpArr.append(NodeMarker())
        }
        arr2d.append(tmpArr)
    }
    return arr2d
}

func isAnyEmptySpot(height:Int, w:Int, arr:[[NodeMarker?]])->Bool{
    var conBlocksNum = 0
    for h in 0..<height{
        if arr[h][w]?.isHidden() == false{
            conBlocksNum += 1
        }else{
            break
        }
    }
    var allBlocksNum = 0
    for h in 0..<height{
        if arr[h][w]?.isHidden() == false{
            allBlocksNum += 1
        }
    }
    return conBlocksNum != allBlocksNum
}

public func pp(_ items: Any..., separator: String = " ", terminator: String = "\n") {
    let output = items.map { "\($0)" }.joined(separator: separator)
    let lnum = "\(#line):"
    Swift.print(lnum + output, terminator: terminator)
}

class AtomicMovement{
    private var indexH:Int = 0
    private var indexW:Int = 0
    private var height:Int = 0
    private var width:Int = 0
    private let lock = DispatchSemaphore(value: 1)
    
    // width and height boundary, e.g. 0-
    init(height:Int = 0, width:Int = 0) {
        self.height = height
        self.width = width
    }
    public func setHW(_ h:Int, _ w:Int){
        lock.wait()
        defer{lock.signal()}
        self.indexH = h 
        self.indexW = w
    }
    public func setPt(_ pt:Pt){
        lock.wait()
        defer{lock.signal()}
        self.indexH = pt.h 
        self.indexW = pt.w 
    }
    public func setW(w:Int){
        lock.wait()
        defer{lock.signal()}
        self.indexW = w
    }
    public func setH(h:Int){
        lock.wait()
        defer {lock.signal()}
        self.indexH = h
    }
    public func getH()->Int{
        lock.wait()
        defer{lock.signal()}
        return indexH
    }
    public func getW()->Int{
        lock.wait()
        defer {lock.signal()}
        return indexW
    }
    
    public func incH(){
        lock.wait()
        defer{lock.signal()}
        if self.indexH < self.height-1 {
            self.indexH += 1
        }
    }

    public func incAndGetH()->Int{
        incH()
        return self.indexH
    }

    public func decH(){
        lock.wait()
        defer{lock.signal()}
        if self.indexH > 0 {
            self.indexH -= 1
        }
    }

    public func decAndGetH()->Int{
        decH()
        return self.indexH
    }
    
    public func incW(){
        lock.wait()
        defer {lock.signal()}
        if self.indexW < self.width - 1 {
            self.indexW += 1
        }
    }
    public func incAndGetW()->Int{
        incW()
        return self.indexW
    }
    
    public func decW(){
        lock.wait()
        defer{lock.signal()}
        if self.indexW > 0 {
        self.indexW -= 1
        }
    }
    
    public func decAndGetW()->Int{
        decW()
        return self.indexW
    }

}

class Queue<T>{
    private let lock = DispatchSemaphore(value: 1)
    private var arr = [T]()
    func dequeue()->T{
        lock.wait()
        defer{ lock.signal()}
        return arr.removeFirst()
    }
    func enqueue(item:T){
        lock.wait()
        defer {
            lock.signal()
        }
        arr.append(item)
    }
    func count()->Int{
       return arr.count
    }
    func printAll(){
        for i in 0..<arr.count{
            print(arr[i])
        }
    }
}

// locker for update function
public enum Update:Int{
    case unlocked = 0
    case locked = 1
}

public enum Lock:Int{
    case unlocked = 0
    case locked = 1
}

public class UpdateLocker{
    private var value:Int = 0
    private let locker = DispatchSemaphore(value: 1)
    public func isLocked()->Bool{
        locker.wait()
        defer{locker.signal()}
        return self.value == 1
    }
    public func lock(){
        locker.wait()
        defer{locker.signal()}
        self.value = 1
    }
    public func unlock(){
        locker.wait()
        defer{locker.signal()}
        self.value = 0
    }
}

class AtomicInteger{
    private var value:Int = 0
    private let lock = DispatchSemaphore(value: 1)
    
    public func get()->Int{
        lock.wait()
        defer{ lock.signal() }
        return value
    }
    public func set(value:Int){
        lock.wait()
        defer{lock.signal()}
        self.value = value
    }
    
    public func incAndGet()->Int{
        lock.wait()
        defer{lock.signal()}
        self.value += 1
        return self.value
    }
    public func decAndGet()->Int{
        lock.wait()
        defer{lock.signal()}
        self.value -= 1
        return self.value
    }
}

class PairSet{
    var height:Int = 0
    var width:Int = 0
    var set = Set<Int>()
    init(height:Int = 0, width:Int) {
        self.height = height
        self.width = width
    }
    func insert(h:Int, w:Int){
        set.insert(h*width + w)
    }
    
    func insert(pair:[Int]){
        insert(h: pair[0], w: pair[1])
    }
    

    func contains(h:Int, w:Int)->Bool{
        let num = h*width + w
        return set.contains(num)
    }
    func contains(pair:[Int])->Bool{
        return contains(h: pair[0], w: pair[1])
    }
    func remove(h:Int, w:Int){
        let num = h*width + w
        set.remove(num)
    }
    func removeAll(){
        set.removeAll()
    }
    func size()->Int{
        return set.count
    }
}

class NodeMarker{
    var isNew:Bool = true
    var isGray:Bool = false
    var block:BlockNode? // = BlockNode()

    init(isNew:Bool = true, isGray:Bool = false, block:BlockNode){
        self.isNew = isNew
        self.isGray = isGray
        self.block = block
    }
    init(){
        self.isNew = true
        self.isGray = false
        self.block = nil
    }
    func isHidden()->Bool{
        return (self.block?.labelNode.isHidden)!
    }
}

class Node{
    init(_ num:Int, isNew:Bool = true, isGray:Bool = false){
        self.num = num
        self.isNew = isNew
        self.isGray = isGray
    }
    var num:Int = 0
    var isNew:Bool = true
    var isGray:Bool = false
}


// KEY: number size, font size, add rectangle
class BlockNode : SKNode{
    //var shapeNode : SKShapeNode = SKShapeNode()
    var labelNode : SKLabelNode = SKLabelNode()
    var rectNode : SKShapeNode = SKShapeNode()
    var isNew:Bool = false
    var isGray:Bool = false
    override init(){
        super.init()
    }

    /**
       XXX3
     
                                       + -> Not been Used
                                       ↓         
     */
    init(position:CGPoint, num:Int, size:CGSize = CGSize(width: 10, height: 10), name:String = "noname"){
        super.init()
        super.position = position
        labelNode.isHidden = true
        labelNode.position = position
        rectNode.isHidden = true
        rectNode.position = position
        //labelNode = createLabel(int:num, position:super.position, fontSize:80.0, name:name)
        rectNode = Shape.rectangle(position: position, size:size)

        // labelNode = createLabel(int:num, position:position, fontSize:80.0, name:name)
        labelNode = createLabel(int:num, position:position, fontSize:size.width, name:name)
    }
    override func removeFromParent() {
        labelNode.removeFromParent()
        rectNode.removeFromParent()
    }
    func setHidden(isHidden:Bool){
        labelNode.isHidden = isHidden
        rectNode.isHidden = isHidden
    }
    func getNum()->Int{
        return Int(labelNode.text!)!
    }
    func setNum(num:Int){
        labelNode.text = String(num)
    }
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}



func parsePath(initPath:[Int], path:[[Int]])->[[Int]]{
    var retArr = [[Int]]()
//    for var arr in split{
//        retArr.append(arr.append(initPath))
//    }
    return retArr
}

// given n pair [h, w] array, compute the numbers of columns
// e.g [1, 2], [4, 2], [5, 2] [7, 4]
// =>{[1, 2], [4, 2], [5, 2]} {[7, 4]} ( ∵ 2 != 4)
func columnCount(path:[[Int]])->Int{
    var set = Set<Int>()
    for p in path{
        set.insert(p[1])
    }
    return set.count
}

// [[[1, 2], [4, 2], [5, 2]], [[2, 4], [5, 2]]] = [[1, 2], [4, 2], [5, 2], [2, 4]]
func uniquePair(h:Int, w:Int, path:[[Int]])->[[Int]]{
    var set = Set<Int>()
    for p in path{
        set.insert(p[0]*w + p[1])
    }
    var arr = [[Int]]()
    for s in set{
        arr.append([s / w, s % w])
    }
    return arr
}

// extract all the blocks sum to 10
func split(arr:[[Int]], separator:[Int])->[[[Int]]]{
    var arr3 = [[[Int]]]()
    var arr2 = [[Int]]()
    for item in arr{
        arr2.append(item)
        if item == separator{
            arr3.append(arr2)
            arr2 = [[Int]]()
        }
    }
    return arr3
}

func recurGridFindSum(h:Int, w:Int, arr: inout [[NodeMarker?]], path:inout [[Int]], sum:Int)->Bool{
    let height = arr.count
    let width = arr[0].count
    print("isNew=\(arr[h][w]?.isNew)")
    print("isHidden=\(arr[h][w]?.isHidden())")
    if arr[h][w]?.isHidden() == false && (arr[h][w]?.isNew)! {
        print(String(format:"[%d][%d]=[%d]", h, w, (arr[h][w]?.block?.getNum())!))
        let s = sum + (arr[h][w]?.block?.getNum())!
        arr[h][w]?.isNew = false
        if s == Constance.SUM10 || s == Constance.SUM20{
            //arr[h][w]?.isGray = true
            print(String(format:"s h=%d w=%d", h, w))
            path.append([h, w])
            return true
        }else if s < Constance.SUM20{
            var c0 = false, c1 = false, c2 = false, c3 = false
            if w < width - 1{
                c0 = recurGridFindSum(h: h, w: w+1, arr:&arr, path:&path, sum:s)
                if c0{
                    //arr[h][w]?.isGray = true
                    print(String(format:"c1 h=%d w=%d", h, w))
                    path.append([h, w])
                }
            }
            if w > 0 {
                c1 = recurGridFindSum(h: h, w: w-1, arr:&arr, path:&path, sum:s)
                if c1{
//                    arr[h][w]?.isGray = true
                    print(String(format:"c1 h=%d w=%d", h, w))
                    path.append([h, w])
                }
            }
            if h < height - 1{
                c2 = recurGridFindSum(h: h+1, w:w, arr:&arr, path:&path, sum:s)
                if c2{
//                    arr[h][w]?.isGray = true
                    print(String(format:"c1 h=%d w=%d", h, w))
                    path.append([h, w])
                }
            }
            if h > 0 {
                c3 = recurGridFindSum(h:h-1, w:w, arr:&arr, path:&path, sum:s)
                if c3{
                    arr[h][w]?.isGray = true
                    print(String(format:"c1 h=%d w=%d", h, w))
                    path.append([h, w])
                }
            }
            return c0 || c1 || c2 || c3
        }
    }
    return false
}

class SmallPlane : SKSpriteNode{
    private var plane : SKSpriteNode = SKSpriteNode()
    private var power : Int = 0
    init(pos: CGPoint, size : CGSize, imageNamed:String, power : Int){
        let texture = SKTexture(imageNamed:"small")
        super.init(texture : texture, color:UIColor.green, size : size)
        
        plane = SKSpriteNode(imageNamed: imageNamed)
        plane.position = pos
        plane.size = size
        self.power = power
    }
    func isAlive() -> Bool{
        return self.power > 0
    }

    override func removeFromParent(){
        self.removeFromParent()
    }
    func getNode() -> SKSpriteNode {
        return plane
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}

class Circle : SKShapeNode{
    var radius:CGFloat = 1.0
    init(position:CGPoint = CGPoint(x:0.0, y:0.0), radius:CGFloat = 10.0) {
        super.init()
        super.position = position
        self.radius = radius
        self.draw()
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    func draw(){
        let path : CGMutablePath = CGMutablePath()
        path.addArc(center: CGPoint.zero,
                    radius: self.radius,
                    startAngle: 0,
                    endAngle: CGFloat.pi * 2,
                    clockwise: true)
        //shapeNode = SKShapeNode(path: path)
        super.path = path.copy()
        super.lineWidth = 1
        super.fillColor = .blue
        super.strokeColor = .white
        super.glowWidth = 0.5
        super.position = self.position
    }
    func click(clickPt:CGPoint)->Bool{
        let n = norm(pt1: self.position, pt2: clickPt)
        if n < self.radius*self.radius{
            return true
        }else{
            return false
        }
    }
    func setColor(color:UIColor){
        super.fillColor = color
    }
    func setRadius(radius:CGFloat){
        self.radius = radius
    }
    func getNode()->SKShapeNode{
        return self
    }
}



class Ball : SKShapeNode{
    var radius:CGFloat = 1.0
    //var position:CGPoint = CGPoint(x:0.0, y:0.0)
    //var shapeNode : SKShapeNode = SKShapeNode()
    //var path : CGMutablePath = CGMutablePath()
    
    init(position:CGPoint = CGPoint(x:0.0, y:0.0), radius:CGFloat = 10.0) {
        super.init()
        super.position = position
        self.radius = radius
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    func draw(){
        let path : CGMutablePath = CGMutablePath()
        path.addArc(center: CGPoint.zero,
                    radius: self.radius,
                    startAngle: 0,
                    endAngle: CGFloat.pi * 2,
                    clockwise: true)
        //shapeNode = SKShapeNode(path: path)
        super.path = path.copy()
        super.lineWidth = 1
        super.fillColor = .blue
        super.strokeColor = .white
        super.glowWidth = 0.5
        super.position = self.position
    }
    func click(clickPt:CGPoint)->Bool{
        let n = norm(pt1: self.position, pt2: clickPt)
        if n < self.radius*self.radius{
            return true
        }else{
            return false
        }
    }
    func setColor(color:UIColor){
        super.fillColor = color
    }
    func setRadius(radius:CGFloat){
        self.radius = radius
    }
    func getNode()->SKShapeNode{
        return self
    }
}

class Shape{
    /**
     * KEY: Create rectangle: center at position, width = CGSize.x height = CGSize.y
     */
    static func rectangle(position:CGPoint, size:CGSize, fill:Bool = true, isHidden:Bool = true)->SKShapeNode{
        let path = UIBezierPath(rect: CGRect(origin: CGPoint(x: -size.width/2, y:-size.height/2), size:size))
        let rect = SKShapeNode(path:path.cgPath)
        rect.lineWidth = 1
        
        if fill {
            rect.fillColor = .gray
        }
        rect.strokeColor = .green
        rect.glowWidth = 0.5
        rect.position = position
        rect.isHidden = isHidden
        return rect
    }

    /* let node = ball(scene:self) self = SKScene
     * self.addChild(node)
     */
    static func ball(radius:CGFloat, position:CGPoint = CGPoint(x: 0.0, y: 0.0))->SKShapeNode{
        let path = CGMutablePath()
        path.addArc(center: CGPoint.zero,
                    radius: radius,
                    startAngle: 0,
                    endAngle: CGFloat.pi * 2,
                    clockwise: true)
        let ball = SKShapeNode(path: path)
        ball.lineWidth = 1
        ball.fillColor = .blue
        ball.strokeColor = .white
        ball.glowWidth = 0.5
        ball.position = position
        return ball
    }
    
    /**
     * Create cirlce: center at = position, radius = radius
     * return SKShapeNode
     */
    static func circle(radius:CGFloat, position:CGPoint = CGPoint(x: 0.0, y: 0.0))->SKShapeNode{
        let path = CGMutablePath()
        path.addArc(center: CGPoint.zero,
                    radius: radius,
                    startAngle: 0,
                    endAngle: CGFloat.pi * 2,
                    clockwise: true)
        let circle = SKShapeNode(path: path)
        circle.lineWidth = 1
        circle.strokeColor = .white
        circle.glowWidth = 0.5
        circle.position = position
        return circle
    }
    
    static func drawCurve(arr:[CGPoint], position:CGPoint)->SKShapeNode{
        let widthPath = UIBezierPath()
        for i in 0..<arr.count-1{
            widthPath.move(to:arr[i])
            widthPath.addLine(to:arr[i+1])
        }
        let lineNode = SKShapeNode(path: widthPath.cgPath)
        lineNode.lineWidth = 1
        lineNode.fillColor = .blue
        lineNode.strokeColor = .green
        lineNode.glowWidth = 2.0
        lineNode.position = position
        return lineNode
    }
}

class Vector4D{
    var x=0.0, y=0.0, z=0.0, w=0.0
    init(x:Double, y:Double, z:Double, w:Double){
        self.x = x;
        self.y = y;
        self.z = z;
        self.w = w;
    }
    func p(){
        print("[x=" + String(x) + " y=" + String(y) + " z=" + String(z) + " w=" + String(w) + "]");
    }
    init(){
    }
}

func +(left:Vector4D, right:Vector4D)->Vector4D{
    let v = Vector4D()
    v.x = left.x + right.x
    v.y = left.y + right.y
    v.z = left.z + right.z
    v.w = left.w + right.w
    return v
}

func -(left:Vector4D, right:Vector4D)->Vector4D{
    let v = Vector4D()
    v.x = left.x - right.x
    v.y = left.y - right.y
    v.z = left.z - right.z
    v.w = left.w - right.w
    return v
}

func dot(left:Vector4D, right:Vector4D)->Double{
    let s = left.x*right.x + left.y*right.y + left.z*right.z + left.w*right.w
    return s
}

// KEY dot product in vector 2d
func dot2(v0 : CGVector, v1:CGVector) -> CGFloat{
    return v0.dx * v1.dx + v0.dy * v1.dy
}

// KEY: rect to four points, to four pts
func fourPts(cen:CGPoint, size:CGSize) -> (topLeft:CGPoint, botLeft:CGPoint, botRight:CGPoint, topRight:CGPoint){
    let topLeft  = CGPoint(x:cen.x - size.width/2, y:cen.y + size.height/2)
    let botLeft  = CGPoint(x:cen.x - size.width/2, y:cen.y - size.height/2)
    let botRight = CGPoint(x:cen.x + size.width/2, y:cen.y - size.height/2)
    let topRight = CGPoint(x:cen.x + size.width/2, y:cen.y + size.height/2)
    return (topLeft, botLeft, botRight, topRight)
}

// KEY: is points inside a triangle, is pt inside a triangle, pt inside triangle
func ptInTriangle(pt:CGPoint, pt0:CGPoint, pt1:CGPoint, pt2:CGPoint) -> (Bool, delta:CGFloat){
    let eps = 1.0e-12
    let a0 = cosVex3(pt0:pt0, pt1:pt, pt2:pt1)
    let a1 = cosVex3(pt0:pt1, pt1:pt, pt2:pt2)
    let a2 = cosVex3(pt0:pt2, pt1:pt, pt2:pt0)
    print("a0 => \(a0)")
    print("a1 => \(a1)")
    print("a2 => \(a2)")
    let s = a0 + a1 + a2
    let diff = abs(2*CGFloat.pi - (a0 + a1 + a2))
    print("a0 + a1 + a2 => \(s)")
    print("diff => \(diff)")
    return (diff < eps, diff)
}
  
func ptInRectangle(pt:CGPoint, cen:CGPoint, size:CGSize) -> Bool{
    let (topLeft, botLeft, botRight, topRight) = fourPts(cen: cen, size:size)
    let (b0, delta0) = ptInTriangle(pt:pt, pt0:topLeft , pt1: botLeft, pt2: botRight)
    let (b1, delta1) = ptInTriangle(pt:pt, pt0: botRight, pt1: topRight, pt2: topLeft)
    return b0 || b1
}

func fun(num:Int){
    print("dog=" + String(num))
}

func fun1(num:Int, str:String)->Int{
    let arr = [1, 2, 3];
    let sum = arr.reduce(0, +);
    
    print("sum=" + String(sum));
    for i in 0..<arr.count{
        print(arr[i])
    }
    
    let strarr = ["dog", "cat"]
    for i in 0..<strarr.count{
        print(strarr[i])
    }
    
    let arr2 = [[1, 2], [2, 3]]
    for i in 0..<arr2.count{
        for j in 0..<arr2[i].count{
            print("arr2=" + String(arr2[i][j]));
        }
    }
    return num;
}

// argument lable
func fun2(number Num:Int, string Str:String){
    print("Num=" + String(Num));
}

// no argument label
func fun3(Num:Int, string:String){
    print("num=" + String(Num) + " " + string)
}

// 1 1 2 3
// 1 2 3 4
func fabonacci(number num:Int)->Int{
    if num == 1 || num == 2{
        return 1
    }
    else{
        return fabonacci(number: num-1) + fabonacci(number: num-2)
    }
}

func geneArray2d(rowLen:Int, colLen:Int)->[[Int]]{
    return Array(repeating: Array(repeating: 0, count: rowLen), count: colLen)
}

func coord(scene:SKScene)->SKShapeNode{
    let coordNode = SKShapeNode.init(rectOf: CGSize.init(width: scene.size.width, height: scene.size.height))
    coordNode.lineWidth = 4.0
    coordNode.strokeColor = UIColor.brown
    
    return coordNode
}


func movePoint(h:Int, w:Int, position:CGPoint, ptArray:[[CGPoint]])->CGPoint{
    let currPt = ptArray[h][w]
    if position.x < currPt.x{
        return ptArray[h][w-1]
    } else if position.x > currPt.x{
        return ptArray[h][w+1]
    }else{
        return ptArray[h][w]
    }
}

func gridPoint(rowLen:Int, colLen:Int, frame:CGRect)->[[CGPoint]]{
    let midX = frame.width/2
    let midY = frame.height/2
    let leftBot = CGPoint(x:frame.midX - midX, y:frame.midY - midY)
    let rightBot = CGPoint(x:frame.midX + midX, y:frame.midY - midY)
    let leftTop = CGPoint(x:frame.midX - midX, y:frame.midY + midY)
    let rightTop = CGPoint(x:frame.midX + midX, y:frame.midY + midY)
    
    let unitWidth:CGFloat = frame.width/CGFloat(rowLen)
    let unitHeight:CGFloat = frame.height/CGFloat(colLen)
    let halfWUnit = CGFloat(Int(unitWidth)/2)
    let halfHUnit = CGFloat(Int(unitHeight/2))
    var botWidth:[CGPoint] = []
    var topWidth:[CGPoint] = []
    
    var leftHeight:[CGPoint] = []
    var rightHeight:[CGPoint] = []
    
    var gridArr = [[CGPoint]]()
    
    // Horizontial lines
    // Top horizontsial line, Bottom horizontial line
    for i in 0..<rowLen + 1{
        topWidth.append(CGPoint(x: leftBot.x + CGFloat(i)*unitWidth, y: leftTop.y))
        botWidth.append(CGPoint(x: leftBot.x + CGFloat(i)*unitWidth, y: leftBot.y))
    }
    
    // Vertical lines
    // Left vertical line, Right vertical line
    for i in 0..<colLen + 1{
        leftHeight.append( CGPoint(x:leftBot.x,  y: leftBot.y + CGFloat(i)*unitHeight))
        rightHeight.append(CGPoint(x:rightBot.x, y: leftBot.y + CGFloat(i)*unitHeight))
    }
    
    // Create two dim grid
    for h in 0..<colLen{
        var tmpArr:[CGPoint] = []
        for w in 0..<rowLen{
            tmpArr.append(CGPoint(x: leftBot.x + halfWUnit + CGFloat(w)*unitWidth, y:leftBot.y + halfHUnit + CGFloat(h)*unitHeight))
        }
        gridArr.append(tmpArr)
    }
    return gridArr
}

func gridLine(numCols:Int, numRows:Int, frame:CGRect)->([CGPoint], [CGPoint], [CGPoint], [CGPoint]){
    let midX = (frame.width/2)
    let midY = (frame.height/2)
    let leftBot = CGPoint(x:frame.midX - midX, y:frame.midY - midY)
    let rightBot = CGPoint(x:frame.midX + midX, y:frame.midY - midY)
    let leftTop = CGPoint(x:frame.midX - midX, y:frame.midY + midY)
    let rightTop = CGPoint(x:frame.midX + midX, y:frame.midY + midY)
    let unitWidth:CGFloat = frame.width/CGFloat(numCols)
    let unitHeight:CGFloat = frame.height/CGFloat(numRows)

    var botWidth:[CGPoint] = []
    var topWidth:[CGPoint] = []
    var leftHeight:[CGPoint] = []
    var rightHeight:[CGPoint] = []

    for i in 0..<numCols + 1{
        topWidth.append(CGPoint(x: leftBot.x + CGFloat(i)*unitWidth, y: leftTop.y))
        botWidth.append(CGPoint(x: leftBot.x + CGFloat(i)*unitWidth, y: leftBot.y))
    }

    // vertical lines
    for i in 0..<numRows + 1{
        leftHeight.append(CGPoint(x:leftBot.x,  y: leftBot.y + CGFloat(i)*unitHeight))
        rightHeight.append(CGPoint(x:rightBot.x, y: leftBot.y + CGFloat(i)*unitHeight))
    }
    return (botWidth, topWidth, leftHeight, rightHeight)
}

    // URL: xfido.com/image/sumblockgame_grid.png
    // KEY: grid color, line color, line width, draw line
    // draw lines fromArr[] to toArr[], assume the size of fromArr and toArr are the same
    func drawLineArray(fromArr:[CGPoint], toArr:[CGPoint], position:CGPoint)->SKShapeNode{
        let widthPath = UIBezierPath()
        for i in 0..<fromArr.count{
            widthPath.move(to:fromArr[i])
            widthPath.addLine(to:toArr[i])
        }
        let lineNode = SKShapeNode(path: widthPath.cgPath)
        lineNode.lineWidth = 1.0
        lineNode.fillColor = .blue
        lineNode.strokeColor = .gray
        lineNode.glowWidth = 1.0
        lineNode.position = position
        return lineNode
    }

func coordItem(widthNum:Int, heightNum:Int, scene:SKScene, clickPt:CGPoint = CGPoint.zero)->[Ball]{
    var gridArr = gridPoint(rowLen:widthNum, colLen:heightNum, frame:scene.frame)
    var nodeArr:[Ball] = []
    for h in 0..<heightNum{
        for w in 0..<widthNum{
            //let dotNode = ball(position: gridArr[h][w])
            let ball = Ball(position: gridArr[h][w], radius:40.0)
            ball.draw()
            if ball.click(clickPt: clickPt){
                ball.setColor(color: .green)
            }
            nodeArr.append(ball)
            //scene.addChild(ball)
        }
    }
    return nodeArr
}
func coordGrid(numCols:Int, numRows:Int, scene:SKScene){
    let (botWidth, topWidth, leftHeight, rightHeight) = gridLine(numCols: numCols, numRows:numRows, frame: scene.frame)
    scene.addChild(drawLineArray(fromArr: botWidth, toArr: topWidth, position:CGPoint(x:scene.frame.midX, y: scene.frame.midY)))
    scene.addChild(drawLineArray(fromArr:leftHeight, toArr:rightHeight, position:CGPoint(x:scene.frame.midX, y: scene.frame.midY)))
}

func coordInfo(scene:SKScene){
    let coordNode = SKShapeNode.init(rectOf: CGSize.init(width: scene.size.width, height: scene.size.height))
    coordNode.lineWidth = 10.0
    coordNode.strokeColor = UIColor.brown
    scene.addChild(coordNode)
    
    let offsetX : CGFloat = 200.0
    let offsetY : CGFloat = 50.0
    let midX = scene.frame.width/2
    let midY = scene.frame.height/2
    let leftBottom = CGPoint(x:scene.frame.midX - midX, y:scene.frame.midY - midY)
    let format = "%.0f" // no decimal
    let leftbot = createLabel(cgpoint: leftBottom, position: CGPoint(x: leftBottom.x + offsetX, y: leftBottom.y + offsetY), format:format)
    let rightTop = CGPoint(x:scene.frame.midX + midX, y:scene.frame.midY + midY)
    let rtLabel = createLabel(cgpoint: rightTop, position: CGPoint(x: rightTop.x - offsetX, y: rightTop.y - offsetY), format:format)
    let leftTop = CGPoint(x: scene.frame.midX - midX, y: scene.frame.midY + midY)
    let leftTopNode = createLabel(cgpoint: leftTop, position: CGPoint(x: leftTop.x + offsetX, y: leftTop.y - offsetY), format:format)
    let rightBot = CGPoint(x: scene.frame.midX + midX, y: scene.frame.midY - midY)
    let rightBotNode = createLabel(cgpoint: rightBot, position: CGPoint(x:rightBot.x - offsetX, y: rightBot.y + offsetY), format:format)
    rtLabel.text = "This is Label from coordInfo"
    rtLabel.isHidden = false;
    scene.addChild(rtLabel)
    scene.addChild(leftbot)
    scene.addChild(leftTopNode)
    scene.addChild(rightBotNode)
}

// localhost:8080/image/game_label.png
// KEY: create label node
func createLabel(str:String, position:CGPoint = CGPoint(x: 0.0, y: 0.0), fontSize:CGFloat = 20.0, name:String = "noname")->SKLabelNode{
    let node = SKLabelNode(fontNamed:"dummy")
    node.text = str
    node.fontSize = fontSize
    node.name = name
    node.fontColor = SKColor.red
    node.zPosition = 1
    node.position = position
    node.horizontalAlignmentMode = .center
    node.verticalAlignmentMode = .center
    node.isHidden = true
    return node
}

func createLabel(float:CGFloat, position:CGPoint = CGPoint(x: 0.0, y: 0.0), fontSize:CGFloat = 20.0, format:String="%.1f")->SKLabelNode{
    let str = String(format:format, float)
    return createLabel(str: str, position: position, fontSize:fontSize)
}

func createLabel(cgpoint:CGPoint, position:CGPoint = CGPoint(x: 0.0, y: 0.0), fontSize:CGFloat = 20.0, format:String="%.1f")->SKLabelNode{
    let str = String(format:format, cgpoint.x) + " " + String(format:format, cgpoint.y)
    return createLabel(str: str, position: position, fontSize:fontSize)
}

func createLabel(int:Int, position:CGPoint = CGPoint(x: 0.0, y: 0.0), fontSize:CGFloat = 20.0, name:String = "noname")->SKLabelNode{
    let str = String(format:"%d", int)
    return createLabel(str: str, position: position, fontSize:fontSize, name:name)
}

func createLabel(double:Double, position:CGPoint = CGPoint(x: 0.0, y: 0.0), fontSize:CGFloat = 20.0)->SKLabelNode{
    let str = double.description
    return createLabel(str: str, position: position, fontSize:fontSize)
}

func createLabel(time:TimeInterval, position:CGPoint = CGPoint(x:0.0, y:0.0), fontSize:CGFloat = 20.0)->SKLabelNode{
    let str = time.description
    return createLabel(str:str, position:position, fontSize:fontSize)
}

// URL: xfido.com/image/sumblockgame_labelnode.png
func labelNode(scene:SKScene)->SKLabelNode{
    let labelNode = SKLabelNode(fontNamed: "Arial")
    labelNode.text = "[" + scene.frame.midX.description + " " + scene.frame.midY.description + "]"
    labelNode.fontSize = 30
    labelNode.name = "backbutton"
    labelNode.fontColor = SKColor.red
    labelNode.zPosition = 1
    labelNode.position = CGPoint(x: scene.frame.midX, y: scene.frame.midY)
    return labelNode
}

/* let node = ball(scene:self) self = SKScene
 * self.addChild(node)
 */
func ball(radius:CGFloat, position:CGPoint = CGPoint(x: 0.0, y: 0.0))->SKShapeNode{
    let path = CGMutablePath()
    path.addArc(center: CGPoint.zero,
                radius: radius,
                startAngle: 0,
                endAngle: CGFloat.pi * 2,
                clockwise: true)
    let ball = SKShapeNode(path: path)
    ball.lineWidth = 1
    ball.fillColor = .blue
    ball.strokeColor = .white
    ball.glowWidth = 0.5
    ball.position = position
    return ball
}

func rectangle(position:CGPoint, size:CGSize)->SKShapeNode{
    let path = UIBezierPath(rect: CGRect(origin: CGPoint(x: -size.width/2, y:-size.height/2), size:size))
    let rect = SKShapeNode(path:path.cgPath)
    rect.lineWidth = 1
    rect.fillColor = .blue
    rect.strokeColor = .white
    rect.glowWidth = 0.5
    rect.position = position
    return rect
}

func ball(position:CGPoint)->SKShapeNode{
    return ball(radius:20.0, position:position)
}

func ball()->SKShapeNode{
    return ball(radius:20.0, position:CGPoint(x: 40.0, y: 40.0))
}

/* let node = createNodeFromPoint(scene:self)  self = SKScene
 * self.addChild(node.linear, node.spline)
 *
 */
func createNodeFromPoint(scene:SKScene)->(linear: SKShapeNode, spline: SKShapeNode){
    var points = [CGPoint(x: 0, y: 0),
                  CGPoint(x: 100, y: 100),
                  CGPoint(x: 200, y: -50),
                  CGPoint(x: 300, y: 30),
                  CGPoint(x: 400, y: 20)]
    let linearShapeNode = SKShapeNode(points: &points,
                                      count: points.count)
    let splineShapeNode = SKShapeNode(splinePoints: &points,
                                      count: points.count)
    return (linearShapeNode, splineShapeNode)
}

// the norm of two points
func norm(pt1: CGPoint, pt2:CGPoint)->CGFloat{
    let n = (pt1.x - pt2.x)*(pt1.x - pt2.x) + (pt1.y - pt2.y)*(pt1.y - pt2.y)
    return n
}

func norm(vec : CGVector) -> CGFloat{
    return norm(pt1 : CGPoint(x:0, y:0), pt2:CGPoint(x:vec.dx, y:vec.dy))
}

func unitVec(vec:CGVector) -> CGVector{
    return normalize(vec: vec)
}
func normalize(vec : CGVector) -> CGVector{
    return vec/norm(vec:vec).squareRoot()
}

func collide(pt1:CGPoint, pt2:CGPoint, dist:CGFloat)->Bool{
    let n = norm(pt1: pt1, pt2: pt2)
    return n <= dist*dist ? true : false
}

func dist(vec:CGVector) -> CGFloat{
    return norm(vec: vec).squareRoot()
}

func dist(pt0:CGPoint, pt1:CGPoint) -> CGFloat{
    return dist(vec:pt0 - pt1)
}

// KEY: CGPoint CGVector arithemtic operator
// p1 - p0 =>  vec(p0 ->  p1)
func -(lhs : CGPoint, rhs : CGPoint) -> CGVector{
    return CGVector(dx : rhs.x - lhs.x, dy : rhs.y - lhs.y)
}

// p0 + v1
func +(lhs : CGPoint, rhs : CGVector) -> CGPoint{
    return CGPointMake(lhs.x + rhs.dx, lhs.y + rhs.dy)
}

func +(lhs : CGPoint, rhs : CGPoint) -> CGPoint{
    return CGPointMake(lhs.x + rhs.x, lhs.y + rhs.y)
}


/*
func ==(lhs : CGPoint, rhs : CGPoint) -> Bool{
    return lhs.x == rhs.x && lhs.y == rhs.y
}
*/

// p0 - v1 => vec
func -(lhs : CGPoint, rhs : CGVector) -> CGPoint{
    return CGPointMake(lhs.x - rhs.dx, lhs.y - rhs.dy)
}

// v0 + v1 => vec
func +(lhs : CGVector, rhs : CGVector) -> CGVector{
    return CGVector(dx:lhs.dx + rhs.dx, dy:lhs.dy + rhs.dy)
}

//  v0 - v1 => vec
func -(lhs : CGVector, rhs : CGVector) -> CGVector{
    return CGVector(dx:lhs.dx - rhs.dx, dy:lhs.dy - rhs.dy)
}

func vec(pt0:CGPoint, pt1:CGPoint) -> CGVector{
    return pt0 - pt1
}

//  a * vec => vec
func *(lhs : Double, rhs : CGVector) -> CGVector{
    return CGVector(dx:lhs * rhs.dx, dy : lhs * rhs.dy)
}
//  vec * a => vec
func *(lhs : CGVector, rhs : Double) -> CGVector{
    return CGVector(dx : lhs.dx * rhs , dy : lhs.dy * rhs)
}

func *(lhs : CGPoint, rhs : Double) -> CGPoint{
    return CGPoint(x : lhs.x * rhs, y : lhs.y * rhs)
}
func *(lhs : Double, rhs : CGPoint) -> CGPoint{
    return CGPoint(x : lhs * rhs.x, y : lhs * rhs.y)
}

// vec/a => vec
func /(lhs: CGVector, rhs : Double) -> CGVector{
    return CGVector(dx : lhs.dx / rhs, dy : lhs.dy / rhs)
}


func nr(vec:CGVector) -> CGFloat{
    let d = vec.dx * vec.dx + vec.dy * vec.dy
    return d.squareRoot()
}

/*
 
 u dot v = |u||v| cos ϕ
 cos ϕ = u dot v / |u||v|
 ϕ = acos u dot v / |u||v|


 // BUG: float point bug
 func cosVex3(pt0:CGPoint, pt1:CGPoint, pt2:CGPoint) -> CGFloat{
    let v10 = vec(pt0:pt1, pt1:pt0)
    let v12 = vec(pt0:pt1, pt1:pt2)
    let d = dot2(v0:v10, v1:v12)
    let u = nr(vec:v10)
    let v = nr(vec:v12)
    return acos (d/(u * v))
}
 
 */
func cosVex3(pt0:CGPoint, pt1:CGPoint, pt2:CGPoint) -> CGFloat{
    let v10 = vec(pt0:pt1, pt1:pt0)
    let v12 = vec(pt0:pt1, pt1:pt2)
    let d = dot2(v0:v10, v1:v12)
    let u = dot2(v0:v10, v1:v10)
    let v = dot2(v0:v12, v1:v12)
    print("v10 => \(v10)")
    print("v12 => \(v12)")
    let xx = d*d/(u*v)
    let x = xx.squareRoot()
    return acos (d < 0 ? -x : x)
}

    

func squareRoot(n:Int)->Double{
    // x^2 - n = 0 
    var a:Double = 1
    var dn = Double(n) 
    var b:Double = Double(n) 
    var m:Double = (a + b)/2.0
    // initial 
    let epsilon = 0.00001
    if n == 1{
        return 1
    }else{
        while m < epsilon{
            m = (a + b)/2.0
            if m*m - dn < 0{
                a = m
            }else{
                b = m
            }
        }
    }
    return m
}

func topBlock(arr:[[Int]])->[[Int]]{
    var ret = [[Int]]()
    var map = [Int : [[Int]]]()
    for hw in arr{
        if var v = map[hw[1]]{
            v.append(hw)
            map[hw[1]] = v
        } else {
            map[hw[1]] = [hw]
        }
    }
    for m in map{
        var max = [0, 0]
        for ar in m.value {
            if ar[0] >= max[0] {
                max = ar 
            }
        }
        ret.append(max)
    }
    return ret
}










